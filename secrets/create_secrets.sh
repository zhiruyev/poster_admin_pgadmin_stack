docker secret create pg_pass.txt freelance_postgre_pass
docker secret create pub_key.txt freelance_pub_key
docker secret create pg_db.txt freelance_postgres_db_name
docker secret create pg_user.txt freelance_postgre_user 
docker secret create secret_key.txt freelance_secret_key
docker secret create priv_key.txt freelance_priv_key
